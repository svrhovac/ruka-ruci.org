CREATE DATABASE  IF NOT EXISTS `ruka_ruci` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `ruka_ruci`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: ruka_ruci
-- ------------------------------------------------------
-- Server version	5.6.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `modx_ng_distribucija`
--

DROP TABLE IF EXISTS `modx_ng_distribucija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modx_ng_distribucija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ugrozni` int(11) NOT NULL,
  `kategorija` int(11) NOT NULL,
  `p_datum` timestamp NULL DEFAULT NULL,
  `p_korsnik` int(11) DEFAULT NULL,
  `p_napomena` text,
  `o_datum` timestamp NULL DEFAULT NULL,
  `o_korisnik` int(11) DEFAULT NULL,
  `o_kolicina` int(11) DEFAULT NULL,
  `d_datum` timestamp NULL DEFAULT NULL,
  `d_korisnik` int(11) DEFAULT NULL,
  `d_kolicina` int(11) DEFAULT NULL,
  `d_napomena` text,
  `d_lat` decimal(10,5) DEFAULT NULL,
  `d_lng` decimal(10,5) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_pomoc_korisnik_idx` (`ugrozni`),
  KEY `fk_pomoc_kategorija1_idx` (`kategorija`),
  CONSTRAINT `fk_pomoc_korisnik` FOREIGN KEY (`ugrozni`) REFERENCES `modx_ng_ugrozeni` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_pomoc_kategorija1` FOREIGN KEY (`kategorija`) REFERENCES `modx_ng_kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modx_ng_distribucija`
--

LOCK TABLES `modx_ng_distribucija` WRITE;
/*!40000 ALTER TABLE `modx_ng_distribucija` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_ng_distribucija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modx_ng_donacije`
--

DROP TABLE IF EXISTS `modx_ng_donacije`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modx_ng_donacije` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `korisnik` int(11) DEFAULT NULL,
  `donator` int(11) NOT NULL,
  `kategorija` int(11) NOT NULL,
  `datum` timestamp NULL DEFAULT NULL,
  `kolicina` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_skladiste_donator1_idx` (`donator`),
  KEY `fk_skladiste_kategorija1_idx` (`kategorija`),
  CONSTRAINT `fk_skladiste_donator1` FOREIGN KEY (`donator`) REFERENCES `modx_ng_donator` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_skladiste_kategorija1` FOREIGN KEY (`kategorija`) REFERENCES `modx_ng_kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modx_ng_donacije`
--

LOCK TABLES `modx_ng_donacije` WRITE;
/*!40000 ALTER TABLE `modx_ng_donacije` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_ng_donacije` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modx_ng_donator`
--

DROP TABLE IF EXISTS `modx_ng_donator`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modx_ng_donator` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `telefon` varchar(80) DEFAULT NULL,
  `napomena` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modx_ng_donator`
--

LOCK TABLES `modx_ng_donator` WRITE;
/*!40000 ALTER TABLE `modx_ng_donator` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_ng_donator` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modx_ng_kategorija`
--

DROP TABLE IF EXISTS `modx_ng_kategorija`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modx_ng_kategorija` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) DEFAULT NULL,
  `opis` text,
  `roditelj` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_kategorija_kategorija1_idx` (`roditelj`),
  CONSTRAINT `fk_kategorija_kategorija1` FOREIGN KEY (`roditelj`) REFERENCES `modx_ng_kategorija` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modx_ng_kategorija`
--

LOCK TABLES `modx_ng_kategorija` WRITE;
/*!40000 ALTER TABLE `modx_ng_kategorija` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_ng_kategorija` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modx_ng_ugrozeni`
--

DROP TABLE IF EXISTS `modx_ng_ugrozeni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modx_ng_ugrozeni` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) DEFAULT NULL,
  `prezime` varchar(45) DEFAULT NULL,
  `adresa` varchar(45) DEFAULT NULL,
  `grad` varchar(45) DEFAULT NULL,
  `posebno_ugrozeni` tinyint(1) DEFAULT NULL,
  `lat` decimal(10,5) DEFAULT NULL,
  `lng` decimal(10,5) DEFAULT NULL,
  `napomena` text,
  `broj_licne_karte` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ime` (`ime`),
  KEY `prezime` (`prezime`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modx_ng_ugrozeni`
--

LOCK TABLES `modx_ng_ugrozeni` WRITE;
/*!40000 ALTER TABLE `modx_ng_ugrozeni` DISABLE KEYS */;
/*!40000 ALTER TABLE `modx_ng_ugrozeni` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-05-24 15:59:11
